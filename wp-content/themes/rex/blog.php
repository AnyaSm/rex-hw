<?php

get_header('blog'); ?>

    <!-- Start blog section -->
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-area">
                        <div class="row">

                            <div class="col-lg-8 col-md-7 col-sm-12">

                                <?php query_posts($query_string . '&cat=-9'); ?>
                                <?php if (have_posts()):
                                    while (have_posts()): the_post(); ?>
                                        <?php setPostViews(get_the_ID()); ?>

                                        <div class="blog-left blog-details">
                                            <!-- Start single blog post -->
                                            <article class="single-from-blog">
                                                <div class="blog-title">
                                                    <h2>
                                                        <a href="<?php the_permalink(); ?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h2>
                                                    <p>Posted by <a class="blog-admin" href="#">admin</a> on <span class="blog-date"><?php the_time( 'F j, Y ' ); ?></span></p>
                                                </div>
                                                <figure>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                                                    </a>
                                                </figure>

                                                <div class="blog-details-content">
                                                    <?php the_content(); ?>
                                                </div>
                                            </article>
                                            <div class="blog-comment">
                                                <h2>Post a comment</h2>
                                                <form class="comment-form" action="">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Name" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="email" placeholder="Enter Email" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea class="form-control"></textarea>
                                                    </div>
                                                    <button class="button button-default" data-text="Comment" type="submit"><span>Comment</span></button>
                                                </form>
                                            </div>
                                            <!-- End single blog post -->
                                        </div>
                                    <?php endwhile; ?>
                                    <!-- End single blog post -->

                                    <!--Start Blog pagination -->
                                    <nav>
                                        <ul class="pagination blog-pagination">
                                            <?php
                                            if (function_exists(custom_pagination)) {
                                                custom_pagination($custom_query->max_num_pages,"",$paged);
                                            }
                                            ?>
                                            <?php wp_reset_postdata(); ?>
                                        </ul>
                                    </nav>
                                    <!-- End blog pagination -->

                                <?php else: ?>
                                    <p>No posts found</p>
                                <?php endif; ?>
                            </div>
                            <?php get_sidebar(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End blog section -->


<?php get_footer(); ?>