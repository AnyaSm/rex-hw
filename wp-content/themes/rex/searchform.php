<div class="search-area">
    <form name="search" action="<?php echo home_url( '/' ) ?>" method="get">
        <input id="search" name="search" type="text" placeholder="What're you looking for?" value="<?php echo get_search_query() ?>">
        <input id="search_submit" value="Rechercher" type="submit">
    </form>
</div>

