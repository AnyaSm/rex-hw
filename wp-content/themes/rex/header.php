<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Rex : Blog</title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <!-- BEGAIN PRELOADER -->
    <div id="preloader">
        <div class="loader">&nbsp;</div>
    </div>
    <!-- END PRELOADER -->

    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

    <!-- Start header section -->
    <header id="header">
        <div class="header-inner">
            <!-- Header image -->
            <?php
            query_posts('p=185');
            while ( have_posts() ) : the_post(); ?>
            <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
            <div class="header-overlay">
                <div class="header-content">
                    <!-- Start header content slider -->
                        <h2 class="header-slide">
                            <?php the_title(); ?>
                        </h2>
                        <span class="tittle-line"></span>
                    <?php endwhile; ?>
                    <!-- End header content slider -->
                    <!-- Header btn area -->
                    <div class="header-btn-area">
                        <a class="knowmore-btn" href="#">KNOW MORE</a>
                        <a class="download-btn" href="#">DOWNLOAD Theme</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End header section -->

    <!-- Start menu section -->
    <section id="menu-area">
        <nav class="navbar navbar-default main-navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- LOGO -->
                    <h1 class="navbar-brand logo">
                        <?php if ( function_exists( 'jetpack_the_site_logo' ) ) jetpack_the_site_logo(); ?>
                    </h1>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'main-nav',
                        'container'       => 'ul',
                        'menu_class'      => 'nav navbar-nav main-nav menu-scroll',
                        'menu_id'         => 'top-menu'
                    ));
                    ?>
                </div><!--/.nav-collapse -->
                <?php the_widget( 'WP_Widget_Search' ); ?>
            </div>
        </nav>
    </section>
    <!-- End menu section -->