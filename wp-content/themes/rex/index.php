
<?php get_header(); ?>

    <!-- Start about section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Start welcome area -->
                    <div class="welcome-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=33');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <div class="welcome-content">
                            <?php
                            $query = new WP_Query( array('post_type' => 'about-reviews', 'posts_per_page' => 100 ) );
                            if ($query->have_posts()):?>
                                <ul class="wc-table">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <li>
                                            <div class="single-wc-content wow fadeInUp">
                                                <?php the_content(); ?>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; wp_reset_postdata(); ?>
                        </div>
                    </div>
                    <!-- End welcome area -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="about-area">
                        <div class="row">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="about-left wow fadeInLeft">
                                    <?php
                                    query_posts('p=75');
                                    while ( have_posts() ) : the_post(); ?>
                                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                                    <?php endwhile; ?>
                                    <a class="introduction-btn" href="<?php the_permalink(); ?>">Introduction</a>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <div class="about-right wow fadeInRight">
                                    <div class="title-area">
                                        <?php
                                        query_posts('p=66');
                                        while ( have_posts() ) : the_post(); ?>
                                            <h2 class="tittle">
                                                <?php the_title(); ?>
                                            </h2>
                                            <span class="tittle-line"></span>
                                            <?php the_content(); ?>
                                        <?php endwhile; ?>
                                        <div class="about-btn-area">
                                            <a href="<?php the_permalink(); ?>" class="button button-default" data-text="KNOW MORE"><span>KNOW MORE</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End about section -->

    <!-- Start call to action -->
    <section id="call-to-action">
        <?php
        query_posts('p=69');
        while ( have_posts() ) : the_post(); ?>
        <?php the_post_thumbnail('full'); ?>
        <div class="call-to-overlay">
            <div class="container">
                <div class="call-to-content wow fadeInUp">
                    <h2 class="tittle">
                        <?php the_title(); ?>
                    </h2>
                    <a href="#" class="button button-default" data-text="GET IT NOW"><span>GET IT NOW</span></a>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End call to action -->

    <!-- Start Team action -->
    <section id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="team-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=45');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- Start team content -->
                        <div class="team-content">
                            <?php
                            $query = new WP_Query( array('post_type' => 'team-reviews', 'posts_per_page' => 100 ) );
                            if ($query->have_posts()):?>
                                <ul class="team-grid">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <li>
                                            <div class="team-item wow fadeInUp" <?php

                                            if ( $thumbnail_id = get_post_thumbnail_id() ) {
                                                if ( $image_src = wp_get_attachment_image_src( $thumbnail_id, 'normal-bg' ) )
                                                    printf( ' style="background: #fff url(%s) no-repeat;"', $image_src[0] );
                                            }

                                            ?>>

                                                <div class="team-info">
                                                    <p>I must explain to you how all this mistaken idea of denouncing pleasure n</p>
                                                    <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>">
                                                        <span class="fa fa-facebook"></span>
                                                    </a>
                                                    <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>">
                                                        <span class="fa fa-twitter"></span>
                                                    </a>
                                                    <a href="<?php echo get_theme_mod('social_links_pinterest'); ?>">
                                                        <span class="fa fa-pinterest"></span>
                                                    </a>
                                                    <a href="#"><span class="fa fa-rss"></span></a>
                                                </div>
                                            </div>
                                            <div class="team-address">
                                                <p><?php the_title(); ?></p>
                                                <?php the_content(); ?>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Start team content -->

    <!-- Start service section -->
    <section id="service">
        <div class="container">
            <div class="row">
                <div>
                    <div class="service-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=92');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- service content -->
                        <div class="service-content">
                            <ul class="service-table">
                                <?php
                                $query = new WP_Query( array('post_type' => 'service-reviews', 'posts_per_page' => 100 ) );
                                if ($query->have_posts()):?>
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <li class="col-md-3 col-sm-6">
                                            <div class="single-service wow slideInUp">
                                                <?php the_content(); ?>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                <?php endif; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End service section -->

    <!-- Start Portfolio section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="portfolio-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=115');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- Portfolio content -->
                        <div class="portfolio-content">
                            <!-- portfolio menu -->
                            <div class="portfolio-menu">
                                <ul>
                                    <li class="filter" data-filter="all">All</li>
                                    <li class="filter" data-filter=".branding">Branding</li>
                                    <li class="filter" data-filter=".design">Design</li>
                                    <li class="filter" data-filter=".development">Development</li>
                                    <li class="filter" data-filter=".photography">Photography</li>
                                </ul>
                            </div>
                            <!-- Portfolio container -->
                            <div id="mixit-container" class="portfolio-container">
                                <?php
                                $query = new WP_Query( array('post_type' => 'portfolio-reviews', 'posts_per_page' => 100 ) );
                                if ($query->have_posts()):?>
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="single-portfolio mix branding">
                                            <div class="single-item">
                                                <?php the_post_thumbnail('full'); ?>
                                                <div class="single-item-content">
                                                    <div class="portfolio-social-icon">
                                                        <a class="fancybox" data-fancybox-group="gallery" href="<?php the_permalink(); ?>"><i class="fa fa-eye"></i></a>
                                                        <a class="link-btn" href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                                                    </div>
                                                    <div class="portfolio-title">
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Portfolio section -->

    <!-- Start Pricing Table section -->
    <section id="pricing-table">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pricing-table-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=119');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- service content -->
                        <div class="pricing-table-content">
                            <?php
                            $query = new WP_Query( array('post_type' => 'price-reviews', 'posts_per_page' => 100 ) );
                            if ($query->have_posts()):?>
                                <ul class="price-table">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <li class="wow slideInUp">
                                            <div class="single-price">
                                                <h4 class="price-header"><?php the_title(); ?></h4>
                                                <?php the_content(); ?>
                                                <a data-text="SIGN UP" class="button button-default" href="#"><span>SIGN UP</span></a>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Pricing Table section -->

<!-- Start Testimonial section -->
<section id="testimonial">
    <?php
    query_posts('p=194');
    while ( have_posts() ) : the_post(); ?>
        <?php the_post_thumbnail('full'); ?>
    <div class="counter-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Start Testimonial area -->
                    <div class="testimonial-area">
                        <div class="title-area">
                            <h2 class="tittle"><?php the_title(); ?></h2>
                            <span class="tittle-line"></span>
                        </div>
                        <?php endwhile; ?>
                        <div class="testimonial-conten">
                            <!-- Start testimonial slider -->
                            <div class="testimonial-slider">
                                <!-- single slide -->
                                <?php
                                $query = new WP_Query( array('post_type' => 'slider-reviews', 'posts_per_page' => 100 ) );
                                if ($query->have_posts()):?>
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="single-slide">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoquat. Duis aute irure d olor in reprehenderit</p>
                                            <div class="single-testimonial">
                                                <?php the_post_thumbnail('full', 'class=img-responsive testimonial-thumb'); ?>
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; wp_reset_postdata(); ?>
                                <!-- single slide -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonial section -->

    <!-- Start from blog section -->
    <section id="from-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="from-blog-area">
                        <div class="title-area">
                            <?php
                            query_posts('p=133');
                            while ( have_posts() ) : the_post(); ?>
                                <h2 class="tittle">
                                    <?php the_title(); ?>
                                </h2>
                                <span class="tittle-line"></span>
                                <?php the_content(); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- From Blog content -->
                        <div class="from-blog-content">
                            <div class="row">

                                <?php
                                // set up or arguments for our custom query (for pagination)
                                $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
                                $query_args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 3,
                                    'paged' => $paged
                                );
                                // create a new instance of WP_Query
                                $the_query = new WP_Query( $query_args );
                                ?>
                                <?php query_posts($query_string . '&cat=-9'); ?>
                                <?php if ( $the_query->have_posts() ) :
                                    while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

                                    <div class="col-md-4">
                                        <article class="single-from-blog">
                                                    <figure>
                                                        <a href="<?php the_permalink(); ?>">
                                                            <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                                                        </a>
                                                    </figure>
                                                    <div class="blog-title">
                                                        <h2>
                                                            <a href="<?php the_permalink(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        </h2>
                                                        <p>Posted by <a class="blog-admin" href="#">admin</a> on <span class="blog-date"><?php the_time( 'F j, Y ' ); ?></span></p>
                                                    </div>
                                                    <?php the_excerpt(); ?>
                                                    <div class="blog-footer">
                                                        <a href="<?php the_permalink() ?>#comments">
                                                            <span class="fa fa-comment"></span>
                                                            <?php comments_number('0 Comments', '1 Comment', '% Comments'); ?>
                                                        </a>
                                                        <a href="<?php the_permalink() ?>#likes">
                                                            <span class="fa fa-thumbs-o-up"></span>
                                                            <?php comments_number('0 Likes', '1 Like', '% Likes'); ?>
                                                        </a>
                                                    </div>
                                            </article>
                                        </div>

                                    <?php endwhile; ?>

                                <?php else: ?>
                                    <p>No posts found</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End from blog section -->




<?php get_footer(); ?>