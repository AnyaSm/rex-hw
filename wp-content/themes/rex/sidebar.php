<div class="col-lg-4 col-md-5 col-sm-12">
    <aside class="blog-right">
        <!-- Start Sidebar Single widget -->
        <div class="single-widget">
            <h2>Search</h2>
            <form class="blog-search">
                <input type="text">
                <button class="button button-default" data-text="Search" type="submit"><span>Search</span></button>
            </form>
        </div>
        <!-- End Sidebar Single widget -->
        <!-- Start Sidebar Single widget -->
        <div class="single-widget">
            <h2>Follow us on</h2>
            <div class="follow-us">
                <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>">
                    <span class="fa fa-facebook"></span>
                </a>
                <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>">
                    <span class="fa fa-twitter"></span>
                </a>
                <a class="google-plus" href="<?php echo get_theme_mod('social_links_google'); ?>">
                    <span class="fa fa-google-plus"></span>
                </a>
                <a class="youtube" href="<?php echo get_theme_mod('social_links_youtube'); ?>">
                    <span class="fa fa-youtube"></span>
                </a>
                <a class="linkedin" href="<?php echo get_theme_mod('social_links_instagram'); ?>">
                    <span class="fa fa-linkedin"></span>
                </a>
                <a class="dribbble" href="<?php echo get_theme_mod('social_links_dribbble'); ?>">
                    <span class="fa fa-dribbble"></span>
                </a>
            </div>
        </div>
        <!-- End Sidebar Single widget -->
        <!-- Start Sidebar Single widget -->
        <div class="single-widget">
            <h2>Popular post</h2>
            <div class="popular-post-widget">
                <div class="media">
                    <?php
                    $args = array( numberposts => 5, meta_key => post_views_count, orderby => meta_value_num, order => DESC );
                    query_posts($args);
                    while ( have_posts() ) : the_post();
                        ?>
                        <div class="media-left">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full', 'class=img-responsive media-object'); ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h4>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
        </div>
        <!-- End Sidebar Single widget -->
        <!-- Start Sidebar Single widget -->
        <div class="single-widget">
            <h2>Instagram feed</h2>
            <div class="instagram-feed">
                <?php
                $query = new WP_Query( array('post_type' => 'instagram-reviews', 'posts_per_page' => 100 ) );
                    if ($query->have_posts()):?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="single-instagram-feed">
                                <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                            </div>
                    <?php endwhile; ?>

                <?php endif; wp_reset_postdata(); ?>
            </div>
        </div>
        <!-- End Sidebar Single widget -->
        <!-- Start Sidebar Single widget -->
        <div class="single-widget">
            <h2>Subscribe to newslater</h2>
            <form class="blog-search">
                <?php echo do_shortcode ('[contact-form-7 id="146" title="Contact form 1"]'); ?>
            </form>
        </div>
        <!-- End Sidebar Single widget -->
    </aside>
</div>