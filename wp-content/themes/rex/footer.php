

    <!-- Start Footer -->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-top-area">
                            <h1 class="footer-logo">
                                <?php if ( function_exists( 'jetpack_the_site_logo' ) ) jetpack_the_site_logo(); ?>
                            </h1>
                            <div class="footer-social">
                                <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>">
                                    <span class="fa fa-facebook"></span>
                                </a>
                                <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>">
                                    <span class="fa fa-twitter"></span>
                                </a>
                                <a class="google-plus" href="<?php echo get_theme_mod('social_links_google'); ?>">
                                    <span class="fa fa-google-plus"></span>
                                </a>
                                <a class="youtube" href="<?php echo get_theme_mod('social_links_youtube'); ?>">
                                    <span class="fa fa-youtube"></span>
                                </a>
                                <a class="linkedin" href="<?php echo get_theme_mod('social_links_instagram'); ?>">
                                    <span class="fa fa-linkedin"></span>
                                </a>
                                <a class="dribbble" href="<?php echo get_theme_mod('social_links_dribbble'); ?>">
                                    <span class="fa fa-dribbble"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <p>Designed by <a href="http://www.markups.io/">MarkUps.io</a></p>
        </div>
    </footer>
    <!-- End Footer -->


    <?php wp_footer(); ?>

</body>
</html>